#Bugs

## Vetsina service nema zkontrolovane objekty na null atributy

#Features

## Improvements
### Lazy loading
- nenatahovat vsechna data najednou

### Efektivita requestu z hibernatu
- podle hibernate statistics api se posila napr. v ramci Leagues.getAll 34 JDBC statementu a posilaji se misto lazy uz dopredu vsechny vyplnene.
- muze to v budoucnu delat problemy s performance
- statistcs api je popsano napr. [tady](https://thorben-janssen.com/how-to-activate-hibernate-statistics-to-analyze-performance-issues/).
