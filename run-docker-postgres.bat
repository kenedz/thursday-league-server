SET CONTAINER_NAME=thursday-league-postgres
SET DB_USER=postgres
SET DB_PASSWORD=postgres
SET DB_NAME=thursday_league

SET SCHEMA_SQL_FILE=1_thursday-league_schema.sql
SET MANDATORY_SQL_FILE=2_thursday-league-mandatory-data.sql
SET DATA_SQL_FILE=3_thursday-league-data.sql

REM pull image
docker pull postgres

REM create container from image postgres with container name specified, exposed port, db pw and db name
docker run --name %CONTAINER_NAME% -p 127.0.0.1:5432:5432 -e POSTGRES_PASSWORD=%DB_PASSWORD% -e POSTGRES_DB=%DB_NAME% -d postgres

REM copy sql files to init db
docker cp src/main/resources/templates/postgresql/%SCHEMA_SQL_FILE% %CONTAINER_NAME%:/%SCHEMA_SQL_FILE%
docker cp src/main/resources/templates/postgresql/%MANDATORY_SQL_FILE% %CONTAINER_NAME%:/%MANDATORY_SQL_FILE%
docker cp src/main/resources/templates/postgresql/%DATA_SQL_FILE% %CONTAINER_NAME%:/%DATA_SQL_FILE%

REM let the container start properly
TIMEOUT 3

REM import sql files into the db
docker exec -u %DB_USER% %CONTAINER_NAME% psql %DB_NAME% %DB_USER% -f /%SCHEMA_SQL_FILE%
docker exec -u %DB_USER% %CONTAINER_NAME% psql %DB_NAME% %DB_USER% -f /%MANDATORY_SQL_FILE%
docker exec -u %DB_USER% %CONTAINER_NAME% psql %DB_NAME% %DB_USER% -f /%DATA_SQL_FILE%

pause
