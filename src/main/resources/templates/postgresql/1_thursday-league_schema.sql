CREATE DATABASE thursday_league
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

-- SEQUENCE: public.matches_lineup_lineup_id_seq

-- DROP SEQUENCE public.matches_lineup_lineup_id_seq;

CREATE SEQUENCE public.matches_lineup_lineup_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.matches_lineup_lineup_id_seq
    OWNER TO postgres;

-- SEQUENCE: public.matches_match_id_seq

-- DROP SEQUENCE public.matches_match_id_seq;

CREATE SEQUENCE public.matches_match_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.matches_match_id_seq
    OWNER TO postgres;

-- SEQUENCE: public.players_players_id_seq

-- DROP SEQUENCE public.players_players_id_seq;

CREATE SEQUENCE public.players_players_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.players_players_id_seq
    OWNER TO postgres;

-- SEQUENCE: public.roles_role_id_seq

-- DROP SEQUENCE public.roles_role_id_seq;

CREATE SEQUENCE public.roles_role_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.roles_role_id_seq
    OWNER TO postgres;

-- SEQUENCE: public.countries_country_id_seq

-- DROP SEQUENCE public.countries_country_id_seq;

CREATE SEQUENCE public.countries_country_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.countries_country_id_seq
    OWNER TO postgres;

-- SEQUENCE: public.sports_sport_id_seq

-- DROP SEQUENCE public.sports_sport_id_seq;

CREATE SEQUENCE public.sports_sport_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.sports_sport_id_seq
    OWNER TO postgres;

-- SEQUENCE: public.leagues_league_id_seq

-- DROP SEQUENCE public.leagues_league_id_seq;

CREATE SEQUENCE public.leagues_league_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.leagues_league_id_seq
    OWNER TO postgres;

-- SEQUENCE: public.seasons_season_id_seq

-- DROP SEQUENCE public.seasons_season_id_seq;

CREATE SEQUENCE public.seasons_season_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.seasons_season_id_seq
    OWNER TO postgres;

-- SEQUENCE: public.user_ranking_ranking_id_seq

-- DROP SEQUENCE public.user_ranking_ranking_id_seq;

CREATE SEQUENCE public.user_ranking_ranking_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.user_ranking_ranking_id_seq
    OWNER TO postgres;

-- SEQUENCE: public.users_user_id_seq

-- DROP SEQUENCE public.users_user_id_seq;

CREATE SEQUENCE public.users_user_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.users_user_id_seq
    OWNER TO postgres;

-- Table: public.roles

-- DROP TABLE public.roles;

CREATE TABLE public.roles
(
    role_id integer NOT NULL DEFAULT nextval('roles_role_id_seq'::regclass),
    role character varying(100) COLLATE pg_catalog."default" NOT NULL,
    description character varying(500) COLLATE pg_catalog."default",
    CONSTRAINT roles_pkey PRIMARY KEY (role_id),
    CONSTRAINT role_id_u UNIQUE (role_id)
)

    TABLESPACE pg_default;

ALTER TABLE public.roles
    OWNER to postgres;

-- Table: public.countries

-- DROP TABLE public.countries;

CREATE TABLE public.countries
(
    country_id integer NOT NULL DEFAULT nextval('countries_country_id_seq'::regclass),
    country_name character varying(150) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT country_id_pk PRIMARY KEY (country_id)
)

    TABLESPACE pg_default;

ALTER TABLE public.countries
    OWNER to postgres;

-- Table: public.sports

-- DROP TABLE public.sports;

CREATE TABLE public.sports
(
    sport_id integer NOT NULL DEFAULT nextval('sports_sport_id_seq'::regclass),
    sport_name character varying(200) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT sport_id_pk PRIMARY KEY (sport_id)
)

    TABLESPACE pg_default;

ALTER TABLE public.sports
    OWNER to postgres;

-- Table: public.leagues

-- DROP TABLE public.leagues;

CREATE TABLE public.leagues
(
    league_id integer NOT NULL DEFAULT nextval('leagues_league_id_seq'::regclass),
    league_name character varying COLLATE pg_catalog."default" NOT NULL,
    sport_id integer NOT NULL,
    country_id integer NOT NULL,
    CONSTRAINT leagues_pkey PRIMARY KEY (league_id),
    CONSTRAINT league_id_u UNIQUE (league_id),
    CONSTRAINT country_id_fk FOREIGN KEY (country_id)
        REFERENCES public.countries (country_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT sport_id_fk FOREIGN KEY (sport_id)
        REFERENCES public.sports (sport_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

    TABLESPACE pg_default;

ALTER TABLE public.leagues
    OWNER to postgres;

-- Table: public.seasons

-- DROP TABLE public.seasons;

CREATE TABLE public.seasons
(
    season_id integer NOT NULL DEFAULT nextval('seasons_season_id_seq'::regclass),
    season_year character varying(20) COLLATE pg_catalog."default",
    season_start timestamp with time zone,
    season_end timestamp with time zone,
    league_id integer NOT NULL,
    CONSTRAINT season_id_pk PRIMARY KEY (season_id),
    CONSTRAINT season_id_u UNIQUE (season_id),
    CONSTRAINT league_id_fk FOREIGN KEY (league_id)
        REFERENCES public.leagues (league_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

    TABLESPACE pg_default;

ALTER TABLE public.seasons
    OWNER to postgres;

-- Table: public.matches

-- DROP TABLE public.matches;

CREATE TABLE public.matches
(
    match_id integer NOT NULL DEFAULT nextval('matches_match_id_seq'::regclass),
    match_date timestamp with time zone NOT NULL,
    home_scored integer,
    away_scored integer,
    season_id integer,
    CONSTRAINT match_id_pk PRIMARY KEY (match_id),
    CONSTRAINT match_id_u UNIQUE (match_id),
    CONSTRAINT season_id_fk FOREIGN KEY (season_id)
        REFERENCES public.seasons (season_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

    TABLESPACE pg_default;

ALTER TABLE public.matches
    OWNER to postgres;

-- Table: public.players

-- DROP TABLE public.players;

CREATE TABLE public.players
(
    player_id integer NOT NULL DEFAULT nextval('players_players_id_seq'::regclass),
    email character varying(200) COLLATE pg_catalog."default" NOT NULL,
    first_name character varying(100) COLLATE pg_catalog."default" NOT NULL,
    last_name character varying(100) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT players_pkey PRIMARY KEY (player_id),
    CONSTRAINT player_email_u UNIQUE (email),
    CONSTRAINT player_id_u UNIQUE (player_id)
)

    TABLESPACE pg_default;

ALTER TABLE public.players
    OWNER to postgres;

-- Table: public.matches_lineup

-- DROP TABLE public.matches_lineup;

CREATE TABLE public.matches_lineup
(
    lineup_id integer NOT NULL DEFAULT nextval('matches_lineup_lineup_id_seq'::regclass),
    match_id integer NOT NULL,
    team character varying(50) COLLATE pg_catalog."default" NOT NULL,
    player_id integer,
    CONSTRAINT matches_lineup_pkey PRIMARY KEY (lineup_id),
    CONSTRAINT match_id_fk FOREIGN KEY (match_id)
        REFERENCES public.matches (match_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT played_id_fk FOREIGN KEY (player_id)
        REFERENCES public.players (player_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

    TABLESPACE pg_default;

ALTER TABLE public.matches_lineup
    OWNER to postgres;
-- Index: fki_played_id_fk

-- DROP INDEX public.fki_played_id_fk;

CREATE INDEX fki_played_id_fk
    ON public.matches_lineup USING btree
        (player_id ASC NULLS LAST)
    TABLESPACE pg_default;

-- Table: public.users

-- DROP TABLE public.users;

CREATE TABLE public.users
(
    user_id integer NOT NULL DEFAULT nextval('users_user_id_seq'::regclass),
    email character varying(200) COLLATE pg_catalog."default" NOT NULL,
    first_name character varying(100) COLLATE pg_catalog."default" NOT NULL,
    last_name character varying(100) COLLATE pg_catalog."default" NOT NULL,
    role_id integer,
    CONSTRAINT users_pkey PRIMARY KEY (user_id),
    CONSTRAINT email_id_u UNIQUE (email),
    CONSTRAINT user_id_u UNIQUE (user_id),
    CONSTRAINT role_id_fk FOREIGN KEY (role_id)
        REFERENCES public.roles (role_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

    TABLESPACE pg_default;

ALTER TABLE public.users
    OWNER to postgres;

-- Table: public.players_ranking

-- DROP TABLE public.players_ranking;

CREATE TABLE public.players_ranking
(
    ranking_id integer NOT NULL DEFAULT nextval('user_ranking_ranking_id_seq'::regclass),
    rank integer,
    points_avg double precision,
    level integer,
    points_overall integer,
    played integer,
    missed integer,
    wins integer,
    draws integer,
    loses integer,
    scored_overall integer,
    scored_avg double precision,
    received_overall integer,
    received_avg double precision,
    player_id integer,
    season_id integer,
    CONSTRAINT player_ranking_pkey PRIMARY KEY (ranking_id),
    CONSTRAINT player_id_fk FOREIGN KEY (player_id)
        REFERENCES public.players (player_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT season_id_fk FOREIGN KEY (season_id)
        REFERENCES public.seasons (season_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

    TABLESPACE pg_default;

ALTER TABLE public.players_ranking
    OWNER to postgres;
-- Index: fki_season_id_fk

-- DROP INDEX public.fki_season_id_fk;

CREATE INDEX fki_season_id_fk
    ON public.players_ranking USING btree
        (season_id ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: fki_user_ranking_user_id_fk

-- DROP INDEX public.fki_user_ranking_user_id_fk;

CREATE INDEX fki_user_ranking_user_id_fk
    ON public.players_ranking USING btree
        (player_id ASC NULLS LAST)
    TABLESPACE pg_default;
