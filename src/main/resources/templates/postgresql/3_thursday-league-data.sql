

INSERT INTO public.seasons (season_id, season_year, season_start, season_end, league_id) VALUES (1, '2018-2019', '2018-09-01 00:00:00+00', '2019-06-30 23:59:59+00', 1);
INSERT INTO public.seasons (season_id, season_year, season_start, season_end, league_id) VALUES (2, '2019-2020', '2019-09-01 00:00:00+00', '2020-06-30 23:59:59+00', 1);

--
-- PostgreSQL database dump
--

--
-- TOC entry 2970 (class 0 OID 16385)
-- Dependencies: 202
-- Data for Name: matches; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.matches (match_id, match_date, home_scored, away_scored, season_id) VALUES (1, '2019-09-05 18:00:00+00', 7, 16, 2);
INSERT INTO public.matches (match_id, match_date, home_scored, away_scored, season_id) VALUES (4, '2019-09-26 18:00:00+00', 13, 4, 2);
INSERT INTO public.matches (match_id, match_date, home_scored, away_scored, season_id) VALUES (5, '2019-10-03 18:00:00+00', 11, 18, 2);
INSERT INTO public.matches (match_id, match_date, home_scored, away_scored, season_id) VALUES (6, '2019-10-10 18:00:00+00', 10, 12, 2);
INSERT INTO public.matches (match_id, match_date, home_scored, away_scored, season_id) VALUES (7, '2019-10-17 18:00:00+00', 9, 13, 2);
INSERT INTO public.matches (match_id, match_date, home_scored, away_scored, season_id) VALUES (8, '2019-10-24 18:00:00+00', 6, 10, 2);
INSERT INTO public.matches (match_id, match_date, home_scored, away_scored, season_id) VALUES (9, '2019-10-31 18:00:00+00', 10, 8, 2);
INSERT INTO public.matches (match_id, match_date, home_scored, away_scored, season_id) VALUES (10, '2019-11-07 18:00:00+00', 10, 6, 2);
INSERT INTO public.matches (match_id, match_date, home_scored, away_scored, season_id) VALUES (11, '2019-11-14 18:00:00+00', 8, 4, 2);
INSERT INTO public.matches (match_id, match_date, home_scored, away_scored, season_id) VALUES (12, '2019-11-21 18:00:00+00', 4, 6, 2);
INSERT INTO public.matches (match_id, match_date, home_scored, away_scored, season_id) VALUES (13, '2019-11-28 18:00:00+00', 11, 9, 2);
INSERT INTO public.matches (match_id, match_date, home_scored, away_scored, season_id) VALUES (14, '2019-12-05 18:00:00+00', 9, 12, 2);
INSERT INTO public.matches (match_id, match_date, home_scored, away_scored, season_id) VALUES (15, '2019-12-12 18:00:00+00', 15, 7, 2);
INSERT INTO public.matches (match_id, match_date, home_scored, away_scored, season_id) VALUES (16, '2019-12-19 18:00:00+00', 10, 5, 2);
INSERT INTO public.matches (match_id, match_date, home_scored, away_scored, season_id) VALUES (17, '2020-01-09 18:00:00+00', 5, 10, 2);
INSERT INTO public.matches (match_id, match_date, home_scored, away_scored, season_id) VALUES (18, '2020-01-16 18:00:00+00', 18, 5, 2);
INSERT INTO public.matches (match_id, match_date, home_scored, away_scored, season_id) VALUES (19, '2020-01-23 18:00:00+00', 4, 8, 2);
INSERT INTO public.matches (match_id, match_date, home_scored, away_scored, season_id) VALUES (20, '2020-01-30 18:00:00+00', 15, 9, 2);
INSERT INTO public.matches (match_id, match_date, home_scored, away_scored, season_id) VALUES (21, '2020-02-06 18:00:00+00', 14, 11, 2);
INSERT INTO public.matches (match_id, match_date, home_scored, away_scored, season_id) VALUES (22, '2020-02-13 18:00:00+00', 6, 4, 2);
INSERT INTO public.matches (match_id, match_date, home_scored, away_scored, season_id) VALUES (23, '2020-02-20 18:00:00+00', 12, 17, 2);
INSERT INTO public.matches (match_id, match_date, home_scored, away_scored, season_id) VALUES (24, '2020-02-27 18:00:00+00', 8, 12, 2);
INSERT INTO public.matches (match_id, match_date, home_scored, away_scored, season_id) VALUES (25, '2020-03-05 18:00:00+00', 5, 17, 2);
INSERT INTO public.matches (match_id, match_date, home_scored, away_scored, season_id) VALUES (26, '2020-03-12 18:00:00+00', 9, 5, 2);
INSERT INTO public.matches (match_id, match_date, home_scored, away_scored, season_id) VALUES (3, '2019-09-19 18:00:00+00', 9, 11, 2);
INSERT INTO public.matches (match_id, match_date, home_scored, away_scored, season_id) VALUES (2, '2019-09-12 18:00:00+00', 7, 9, 2);



--
-- TOC entry 2971 (class 0 OID 16388)
-- Dependencies: 203
-- Data for Name: matches_lineup; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (1, 1, 'home', 4);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (2, 1, 'home', 9);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (3, 1, 'home', 10);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (4, 1, 'home', 3);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (5, 1, 'home', 2);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (6, 1, 'away', 7);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (7, 1, 'away', 13);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (8, 1, 'away', 1);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (9, 1, 'away', 5);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (10, 1, 'away', 17);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (29, 4, 'home', 11);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (30, 4, 'home', 4);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (31, 4, 'home', 7);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (32, 4, 'home', 13);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (33, 4, 'home', 5);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (34, 4, 'home', 2);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (35, 4, 'away', 10);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (36, 4, 'away', 9);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (37, 4, 'away', 12);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (38, 4, 'away', 3);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (39, 4, 'away', 1);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (40, 4, 'away', 17);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (41, 5, 'home', 6);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (42, 5, 'home', 3);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (43, 5, 'home', 10);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (44, 5, 'home', 13);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (45, 5, 'home', 2);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (46, 5, 'away', 7);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (47, 5, 'away', 9);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (48, 5, 'away', 12);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (49, 5, 'away', 5);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (50, 5, 'away', 1);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (51, 5, 'away', 17);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (52, 6, 'home', 7);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (53, 6, 'home', 3);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (54, 6, 'home', 8);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (55, 6, 'home', 1);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (56, 6, 'home', 5);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (57, 6, 'home', 17);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (58, 6, 'away', 10);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (59, 6, 'away', 9);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (60, 6, 'away', 12);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (61, 6, 'away', 13);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (62, 6, 'away', 2);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (63, 7, 'home', 11);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (64, 7, 'home', 4);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (65, 7, 'home', 12);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (66, 7, 'home', 8);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (67, 7, 'home', 2);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (68, 7, 'home', 17);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (69, 7, 'away', 9);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (70, 7, 'away', 7);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (71, 7, 'away', 13);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (72, 7, 'away', 3);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (73, 7, 'away', 1);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (74, 7, 'away', 5);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (75, 8, 'home', 4);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (76, 8, 'home', 9);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (77, 8, 'home', 3);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (78, 8, 'home', 10);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (79, 8, 'home', 5);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (80, 8, 'away', 7);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (81, 8, 'away', 12);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (82, 8, 'away', 8);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (83, 8, 'away', 1);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (84, 8, 'away', 2);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (85, 9, 'home', 11);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (86, 9, 'home', 4);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (87, 9, 'home', 7);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (88, 9, 'home', 10);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (89, 9, 'home', 13);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (90, 9, 'home', 5);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (91, 9, 'home', 2);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (92, 9, 'away', 6);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (93, 9, 'away', 3);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (94, 9, 'away', 12);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (95, 9, 'away', 8);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (96, 9, 'away', 1);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (97, 9, 'away', 17);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (98, 10, 'home', 11);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (99, 10, 'home', 10);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (100, 10, 'home', 5);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (101, 10, 'home', 2);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (102, 10, 'away', 4);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (103, 10, 'away', 13);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (104, 10, 'away', 8);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (105, 10, 'away', 1);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (106, 11, 'home', 11);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (107, 11, 'home', 7);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (108, 11, 'home', 8);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (109, 11, 'home', 13);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (110, 11, 'home', 2);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (111, 11, 'away', 12);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (112, 11, 'away', 10);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (113, 11, 'away', 3);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (114, 11, 'away', 5);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (115, 11, 'away', 17);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (116, 12, 'home', 11);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (117, 12, 'home', 7);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (118, 12, 'home', 4);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (119, 12, 'home', 13);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (120, 12, 'home', 5);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (121, 12, 'away', 6);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (122, 12, 'away', 10);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (123, 12, 'away', 1);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (124, 12, 'away', 2);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (125, 13, 'home', 6);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (126, 13, 'home', 8);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (127, 13, 'home', 1);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (128, 13, 'home', 17);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (129, 13, 'away', 7);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (130, 13, 'away', 4);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (131, 13, 'away', 3);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (132, 13, 'away', 13);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (133, 13, 'away', 5);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (134, 14, 'home', 4);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (135, 14, 'home', 12);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (136, 14, 'home', 8);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (137, 14, 'home', 1);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (138, 14, 'home', 5);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (139, 14, 'away', 10);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (140, 14, 'away', 7);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (141, 14, 'away', 3);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (142, 14, 'away', 2);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (143, 15, 'home', 4);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (144, 15, 'home', 12);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (145, 15, 'home', 10);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (146, 15, 'home', 1);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (147, 15, 'home', 2);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (148, 15, 'away', 7);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (149, 15, 'away', 8);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (150, 15, 'away', 13);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (151, 15, 'away', 5);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (152, 15, 'away', 17);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (153, 16, 'home', 7);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (154, 16, 'home', 12);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (155, 16, 'home', 10);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (156, 16, 'home', 8);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (157, 16, 'home', 1);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (158, 16, 'away', 9);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (159, 16, 'away', 3);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (160, 16, 'away', 16);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (161, 16, 'away', 2);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (162, 17, 'home', 7);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (163, 17, 'home', 9);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (164, 17, 'home', 12);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (165, 17, 'home', 13);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (166, 17, 'away', 3);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (167, 17, 'away', 10);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (168, 17, 'away', 1);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (169, 17, 'away', 5);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (170, 18, 'home', 6);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (171, 18, 'home', 7);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (172, 18, 'home', 8);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (173, 18, 'home', 10);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (174, 18, 'away', 9);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (175, 18, 'away', 4);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (176, 18, 'away', 12);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (177, 18, 'away', 3);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (178, 18, 'away', 13);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (179, 19, 'home', 6);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (180, 19, 'home', 11);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (181, 19, 'home', 3);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (182, 19, 'home', 2);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (183, 19, 'away', 7);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (184, 19, 'away', 10);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (185, 19, 'away', 17);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (186, 19, 'away', 5);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (187, 20, 'home', 11);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (188, 20, 'home', 9);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (189, 20, 'home', 10);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (190, 20, 'home', 1);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (191, 20, 'away', 7);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (192, 20, 'away', 3);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (193, 20, 'away', 2);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (194, 20, 'away', 5);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (195, 21, 'home', 6);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (196, 21, 'home', 11);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (197, 21, 'home', 4);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (198, 21, 'home', 12);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (199, 21, 'home', 1);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (200, 21, 'away', 7);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (201, 21, 'away', 8);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (202, 21, 'away', 3);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (203, 21, 'away', 5);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (204, 21, 'away', 17);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (205, 21, 'away', 2);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (206, 22, 'home', 11);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (207, 22, 'home', 4);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (208, 22, 'home', 10);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (209, 22, 'home', 9);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (210, 22, 'home', 2);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (211, 22, 'away', 6);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (212, 22, 'away', 7);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (213, 22, 'away', 3);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (214, 22, 'away', 5);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (215, 23, 'home', 11);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (216, 23, 'home', 6);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (217, 23, 'home', 4);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (218, 23, 'home', 9);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (219, 23, 'home', 3);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (220, 23, 'away', 10);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (221, 23, 'away', 12);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (222, 23, 'away', 13);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (223, 23, 'away', 8);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (224, 23, 'away', 1);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (225, 23, 'away', 2);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (226, 24, 'home', 11);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (227, 24, 'home', 4);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (228, 24, 'home', 10);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (229, 24, 'home', 9);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (230, 24, 'home', 2);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (231, 24, 'home', 17);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (232, 24, 'away', 7);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (233, 24, 'away', 3);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (234, 24, 'away', 12);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (235, 24, 'away', 8);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (236, 24, 'away', 1);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (237, 24, 'away', 5);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (238, 25, 'home', 11);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (239, 25, 'home', 7);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (240, 25, 'home', 3);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (241, 25, 'home', 2);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (242, 25, 'away', 4);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (243, 25, 'away', 10);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (244, 25, 'away', 1);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (245, 25, 'away', 5);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (246, 26, 'home', 11);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (247, 26, 'home', 4);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (248, 26, 'home', 10);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (249, 26, 'home', 2);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (250, 26, 'away', 6);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (251, 26, 'away', 7);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (252, 26, 'away', 8);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (253, 26, 'away', 5);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (254, 3, 'home', 18);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (255, 3, 'home', 4);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (256, 3, 'home', 3);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (257, 3, 'home', 8);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (258, 3, 'home', 1);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (259, 3, 'home', 2);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (260, 3, 'away', 7);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (261, 3, 'away', 10);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (262, 3, 'away', 9);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (263, 3, 'away', 12);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (264, 3, 'away', 5);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (265, 2, 'home', 11);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (266, 2, 'home', 10);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (267, 2, 'home', 3);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (268, 2, 'home', 2);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (269, 2, 'home', 13);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (270, 2, 'away', 18);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (271, 2, 'away', 4);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (272, 2, 'away', 7);
INSERT INTO public.matches_lineup (lineup_id, match_id, team, player_id) VALUES (273, 2, 'away', 17);


--
-- TOC entry 2976 (class 0 OID 16400)
-- Dependencies: 208
-- Data for Name: players_ranking; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.players_ranking (ranking_id, rank, points_avg, level, points_overall, played, missed, wins, draws, loses, scored_overall, scored_avg, received_overall, received_avg, player_id, season_id) VALUES (1, 1, 1.4, 10, 28, 20, 6, 14, 0, 6, 230, 11.5, 176, 8.8, 1, 2);
INSERT INTO public.players_ranking (ranking_id, rank, points_avg, level, points_overall, played, missed, wins, draws, loses, scored_overall, scored_avg, received_overall, received_avg, player_id, season_id) VALUES (2, 2, 1.391304347826087, 10, 32, 23, 3, 16, 0, 7, 233, 10.130434782608695, 193, 8.391304347826088, 10, 2);
INSERT INTO public.players_ranking (ranking_id, rank, points_avg, level, points_overall, played, missed, wins, draws, loses, scored_overall, scored_avg, received_overall, received_avg, player_id, season_id) VALUES (3, 3, 1.1666666666666667, 9, 28, 24, 2, 14, 0, 10, 237, 9.875, 211, 8.791666666666666, 7, 2);
INSERT INTO public.players_ranking (ranking_id, rank, points_avg, level, points_overall, played, missed, wins, draws, loses, scored_overall, scored_avg, received_overall, received_avg, player_id, season_id) VALUES (4, 4, 1.125, 9, 18, 16, 10, 9, 0, 7, 163, 10.1875, 163, 10.1875, 12, 2);
INSERT INTO public.players_ranking (ranking_id, rank, points_avg, level, points_overall, played, missed, wins, draws, loses, scored_overall, scored_avg, received_overall, received_avg, player_id, season_id) VALUES (5, 5, 1.0666666666666667, 8, 16, 15, 11, 8, 0, 7, 134, 8.933333333333334, 133, 8.866666666666667, 11, 2);
INSERT INTO public.players_ranking (ranking_id, rank, points_avg, level, points_overall, played, missed, wins, draws, loses, scored_overall, scored_avg, received_overall, received_avg, player_id, season_id) VALUES (6, 6, 1.0434782608695652, 7, 24, 23, 3, 12, 0, 11, 213, 9.26086956521739, 222, 9.652173913043478, 2, 2);
INSERT INTO public.players_ranking (ranking_id, rank, points_avg, level, points_overall, played, missed, wins, draws, loses, scored_overall, scored_avg, received_overall, received_avg, player_id, season_id) VALUES (7, 7, 1, 7, 22, 22, 4, 11, 0, 11, 216, 9.818181818181818, 194, 8.818181818181818, 5, 2);
INSERT INTO public.players_ranking (ranking_id, rank, points_avg, level, points_overall, played, missed, wins, draws, loses, scored_overall, scored_avg, received_overall, received_avg, player_id, season_id) VALUES (8, 7, 1, 7, 2, 2, 24, 1, 0, 1, 18, 9, 18, 9, 18, 2);
INSERT INTO public.players_ranking (ranking_id, rank, points_avg, level, points_overall, played, missed, wins, draws, loses, scored_overall, scored_avg, received_overall, received_avg, player_id, season_id) VALUES (9, 9, 0.9333333333333333, 5, 14, 15, 11, 7, 0, 8, 143, 9.533333333333333, 151, 10.066666666666666, 13, 2);
INSERT INTO public.players_ranking (ranking_id, rank, points_avg, level, points_overall, played, missed, wins, draws, loses, scored_overall, scored_avg, received_overall, received_avg, player_id, season_id) VALUES (10, 10, 0.875, 5, 14, 16, 10, 7, 0, 9, 160, 10, 155, 9.6875, 8, 2);
INSERT INTO public.players_ranking (ranking_id, rank, points_avg, level, points_overall, played, missed, wins, draws, loses, scored_overall, scored_avg, received_overall, received_avg, player_id, season_id) VALUES (11, 11, 0.8571428571428571, 4, 12, 14, 12, 6, 0, 8, 127, 9.071428571428571, 158, 11.285714285714286, 9, 2);
INSERT INTO public.players_ranking (ranking_id, rank, points_avg, level, points_overall, played, missed, wins, draws, loses, scored_overall, scored_avg, received_overall, received_avg, player_id, season_id) VALUES (12, 12, 0.8421052631578947, 3, 16, 19, 7, 8, 0, 11, 177, 9.31578947368421, 187, 9.842105263157896, 4, 2);
INSERT INTO public.players_ranking (ranking_id, rank, points_avg, level, points_overall, played, missed, wins, draws, loses, scored_overall, scored_avg, received_overall, received_avg, player_id, season_id) VALUES (13, 13, 0.8, 3, 8, 10, 16, 4, 0, 6, 93, 9.3, 97, 9.7, 6, 2);
INSERT INTO public.players_ranking (ranking_id, rank, points_avg, level, points_overall, played, missed, wins, draws, loses, scored_overall, scored_avg, received_overall, received_avg, player_id, season_id) VALUES (14, 14, 0.7692307692307693, 2, 10, 13, 13, 5, 0, 8, 123, 9.461538461538462, 135, 10.384615384615385, 17, 2);
INSERT INTO public.players_ranking (ranking_id, rank, points_avg, level, points_overall, played, missed, wins, draws, loses, scored_overall, scored_avg, received_overall, received_avg, player_id, season_id) VALUES (15, 15, 0.36363636363636365, 1, 8, 22, 4, 4, 0, 18, 177, 8.045454545454545, 254, 11.545454545454545, 3, 2);
INSERT INTO public.players_ranking (ranking_id, rank, points_avg, level, points_overall, played, missed, wins, draws, loses, scored_overall, scored_avg, received_overall, received_avg, player_id, season_id) VALUES (16, 16, 0, 1, 0, 0, 26, 0, 0, 0, 0, 0, 0, 0, 14, 2);
INSERT INTO public.players_ranking (ranking_id, rank, points_avg, level, points_overall, played, missed, wins, draws, loses, scored_overall, scored_avg, received_overall, received_avg, player_id, season_id) VALUES (17, 16, 0, 1, 0, 0, 26, 0, 0, 0, 0, 0, 0, 0, 15, 2);
INSERT INTO public.players_ranking (ranking_id, rank, points_avg, level, points_overall, played, missed, wins, draws, loses, scored_overall, scored_avg, received_overall, received_avg, player_id, season_id) VALUES (18, 16, 0, 1, 0, 1, 25, 0, 0, 1, 5, 5, 10, 10, 16, 2);

-- SEASONS

SELECT pg_catalog.setval('public.seasons_season_id_seq', 2, true);


--
-- TOC entry 2993 (class 0 OID 0)
-- Dependencies: 204
-- Name: matches_lineup_lineup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.matches_lineup_lineup_id_seq', 273, true);


--
-- TOC entry 2994 (class 0 OID 0)
-- Dependencies: 205
-- Name: matches_match_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.matches_match_id_seq', 26, true);

--
-- TOC entry 2997 (class 0 OID 0)
-- Dependencies: 211
-- Name: user_ranking_ranking_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_ranking_ranking_id_seq', 18, true);
