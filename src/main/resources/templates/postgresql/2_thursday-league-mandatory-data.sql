--
-- PostgreSQL database dump
--


--
-- TOC entry 2977 (class 0 OID 16403)
-- Dependencies: 209
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.roles (role_id, role, description) VALUES (1, 'superadmin', 'unremovable');
INSERT INTO public.roles (role_id, role, description) VALUES (2, 'admin', 'removable, full rights');
INSERT INTO public.roles (role_id, role, description) VALUES (3, 'user', 'removable, limited rights');


-- SPORTS

INSERT INTO public.sports (sport_id, sport_name) VALUES (1, 'futsal');


-- COUNTRIES

INSERT INTO public.countries (country_id, country_name) VALUES (1, 'Czech Republic');


-- COUNTRIES

INSERT INTO public.leagues (league_id, league_name, sport_id, country_id) VALUES (1, 'Thursday League', 1, 1);


--
-- TOC entry 2980 (class 0 OID 16413)
-- Dependencies: 212
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.users (user_id, email, first_name, last_name, role_id) VALUES (1, 'zd.tinka@gmail.com', 'Zdeněk', 'Tinka', 1);
INSERT INTO public.users (user_id, email, first_name, last_name, role_id) VALUES (2, 'plechon@gmail.com', 'Vojtěch', 'Šobáň', 1);
INSERT INTO public.users (user_id, email, first_name, last_name, role_id) VALUES (3, 'mir.krat.73@gmail.com', 'Miroslav', 'Kral', 3);
INSERT INTO public.users (user_id, email, first_name, last_name, role_id) VALUES (4, 'ondrej.jezek@gmail.com', 'Ondrej', 'Jezek', 3);
INSERT INTO public.users (user_id, email, first_name, last_name, role_id) VALUES (5, 'roman.miklus@gmail.com', 'Roman', 'Miklus', 3);
INSERT INTO public.users (user_id, email, first_name, last_name, role_id) VALUES (6, 'daniel.kalich@gmail.com', 'Daniel', 'Kalich', 3);
INSERT INTO public.users (user_id, email, first_name, last_name, role_id) VALUES (7, 'jakub.smidek@gmail.com', 'Jakub', 'Smidek', 3);
INSERT INTO public.users (user_id, email, first_name, last_name, role_id) VALUES (8, 'pluhjan@gmail.com', 'Jan', 'Pluhar', 3);
INSERT INTO public.users (user_id, email, first_name, last_name, role_id) VALUES (9, 'jansoban@gmail.com', 'Jan', 'Soban', 3);
INSERT INTO public.users (user_id, email, first_name, last_name, role_id) VALUES (10, 'michall.laska@gmail.com', 'Michal', 'Laska', 3);
INSERT INTO public.users (user_id, email, first_name, last_name, role_id) VALUES (11, 'obazi.102@gmail.com', 'Ondrej', 'Bazala', 3);
INSERT INTO public.users (user_id, email, first_name, last_name, role_id) VALUES (12, 'peterpassak@gmail.com', 'Peter', 'Passak', 3);
INSERT INTO public.users (user_id, email, first_name, last_name, role_id) VALUES (13, 'petoduriac@gmail.com', 'Peter', 'Duriac', 3);
INSERT INTO public.users (user_id, email, first_name, last_name, role_id) VALUES (14, 'radim.kalich@gmail.com', 'Radim', 'Kalich', 3);
INSERT INTO public.users (user_id, email, first_name, last_name, role_id) VALUES (15, 'lpolach@centrum.cz', 'Libor', 'Polach', 3);
INSERT INTO public.users (user_id, email, first_name, last_name, role_id) VALUES (16, 'tinka.t@email.cz', 'Tomas', 'Tinka', 3);
INSERT INTO public.users (user_id, email, first_name, last_name, role_id) VALUES (17, 'tomas.soba@centrum.cz', 'Tomas', 'Soba', 3);
INSERT INTO public.users (user_id, email, first_name, last_name, role_id) VALUES (18, 'jiri.geryk@seznam.cz', 'Jiri', 'Geryk', 3);


--
-- TOC entry 2974 (class 0 OID 16395)
-- Dependencies: 206
-- Data for Name: players; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.players (player_id, email, first_name, last_name) VALUES (1, 'zd.tinka@gmail.com', 'Zdeněk', 'Tinka');
INSERT INTO public.players (player_id, email, first_name, last_name) VALUES (2, 'plechon@gmail.com', 'Vojtěch', 'Šobáň');
INSERT INTO public.players (player_id, email, first_name, last_name) VALUES (3, 'mir.krat.73@gmail.com', 'Miroslav', 'Kral');
INSERT INTO public.players (player_id, email, first_name, last_name) VALUES (4, 'ondrej.jezek@gmail.com', 'Ondrej', 'Jezek');
INSERT INTO public.players (player_id, email, first_name, last_name) VALUES (5, 'roman.miklus@gmail.com', 'Roman', 'Miklus');
INSERT INTO public.players (player_id, email, first_name, last_name) VALUES (6, 'daniel.kalich@gmail.com', 'Daniel', 'Kalich');
INSERT INTO public.players (player_id, email, first_name, last_name) VALUES (7, 'jakub.smidek@gmail.com', 'Jakub', 'Smidek');
INSERT INTO public.players (player_id, email, first_name, last_name) VALUES (8, 'pluhjan@gmail.com', 'Jan', 'Pluhar');
INSERT INTO public.players (player_id, email, first_name, last_name) VALUES (9, 'jansoban@gmail.com', 'Jan', 'Soban');
INSERT INTO public.players (player_id, email, first_name, last_name) VALUES (10, 'michall.laska@gmail.com', 'Michal', 'Laska');
INSERT INTO public.players (player_id, email, first_name, last_name) VALUES (11, 'obazi.102@gmail.com', 'Ondrej', 'Bazala');
INSERT INTO public.players (player_id, email, first_name, last_name) VALUES (12, 'peterpassak@gmail.com', 'Peter', 'Passak');
INSERT INTO public.players (player_id, email, first_name, last_name) VALUES (13, 'petoduriac@gmail.com', 'Peter', 'Duriac');
INSERT INTO public.players (player_id, email, first_name, last_name) VALUES (14, 'radim.kalich@gmail.com', 'Radim', 'Kalich');
INSERT INTO public.players (player_id, email, first_name, last_name) VALUES (15, 'lpolach@centrum.cz', 'Libor', 'Polach');
INSERT INTO public.players (player_id, email, first_name, last_name) VALUES (16, 'tinka.t@email.cz', 'Tomas', 'Tinka');
INSERT INTO public.players (player_id, email, first_name, last_name) VALUES (17, 'tomas.soba@centrum.cz', 'Tomas', 'Soba');
INSERT INTO public.players (player_id, email, first_name, last_name) VALUES (18, 'jiri.geryk@seznam.cz', 'Jiri', 'Geryk');


--
-- TOC entry 2995 (class 0 OID 0)
-- Dependencies: 207
-- Name: players_players_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.players_players_id_seq', 18, true);


--
-- TOC entry 2996 (class 0 OID 0)
-- Dependencies: 210
-- Name: roles_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.roles_role_id_seq', 3, true);


-- SPORTS SEQUENCE

SELECT pg_catalog.setval('public.sports_sport_id_seq', 1, true);


-- SPORTS SEQUENCE

SELECT pg_catalog.setval('public.countries_country_id_seq', 1, true);


-- LEAGUES SEQUENCE

SELECT pg_catalog.setval('public.leagues_league_id_seq', 1, true);


--
-- TOC entry 2998 (class 0 OID 0)
-- Dependencies: 213
-- Name: users_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_user_id_seq', 18, true);
