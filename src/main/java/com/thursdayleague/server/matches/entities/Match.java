package com.thursdayleague.server.matches.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.thursdayleague.server.matcheslineup.entities.Lineup;
import com.thursdayleague.server.seasons.entities.Season;

import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "matches")
public class Match implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "match_id", nullable = false)
    private Integer matchId;

    @Column(name = "match_date", nullable = false)
    private OffsetDateTime matchDate;

    @Column(name = "home_scored")
    private Integer homeScored;

    @Column(name = "away_scored")
    private Integer awayScored;

    @OneToMany(mappedBy = "match", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Lineup> lineup;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "season_id", nullable = false)
    private Season season;

    @Transient
    private Integer seasonId;

    public Integer getMatchId() {
        return matchId;
    }

    public void setMatchId(Integer matchId) {
        this.matchId = matchId;
    }

    public OffsetDateTime getMatchDate() {
        return matchDate;
    }

    public void setMatchDate(OffsetDateTime matchDate) {
        this.matchDate = matchDate;
    }

    public Integer getHomeScored() {
        return homeScored;
    }

    public void setHomeScored(Integer homeScored) {
        this.homeScored = homeScored;
    }

    public Integer getAwayScored() {
        return awayScored;
    }

    public void setAwayScored(Integer awayScored) {
        this.awayScored = awayScored;
    }

    public List<Lineup> getLineup() {
        return lineup;
    }

    public void setLineup(List<Lineup> lineup) {
        this.lineup = lineup;
    }

    public Season getSeason() {
        return season;
    }

    public void setSeason(Season season) {
        this.season = season;
    }

    public Integer getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(Integer seasonId) {
        this.seasonId = seasonId;
    }
}
