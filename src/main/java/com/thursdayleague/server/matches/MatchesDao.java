package com.thursdayleague.server.matches;

import com.thursdayleague.server.matches.entities.Match;
import com.thursdayleague.server.ranking.entities.Ranking;

import java.sql.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MatchesDao extends JpaRepository<Match, Integer> {

	String GET_MATCHES_BY_SEASON_ID = "SELECT * FROM matches WHERE season_id = :seasonId";
	String DELETE_MATCHES_BY_SEASON_ID = "DELETE FROM matches WHERE season_id = :seasonId";

	@Query(value = GET_MATCHES_BY_SEASON_ID, nativeQuery = true)
	List<Match> getMatchesBySeasonId(@Param("seasonId") final Integer seasonId);

	@Modifying
	@Query(value = DELETE_MATCHES_BY_SEASON_ID, nativeQuery = true)
	void deleteMatchesBySeasonId(@Param("seasonId") final Integer seasonId);
}
