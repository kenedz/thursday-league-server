package com.thursdayleague.server.matches;

import com.thursdayleague.server.matches.entities.Match;
import com.thursdayleague.server.matcheslineup.LineupService;
import com.thursdayleague.server.matcheslineup.entities.Lineup;
import com.thursdayleague.server.seasons.SeasonsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.List;

import javax.transaction.Transactional;

@Service
public class MatchesService {

    @Autowired
    private MatchesDao matchesDao;

    @Autowired
    private LineupService lineupService;

    @Autowired
    private SeasonsService seasonsService;

    public List<Match> getMatchesBySeasonId(Integer seasonId) {
        List<Match> matches = matchesDao.getMatchesBySeasonId(seasonId);
        for (Match match : matches) {
            updateBySeasonId(match);
        }
        return matches;
    }

    @Transactional
    public Match createMatch(Match match) {
        Match matchWithoutLineup = copyMatchWithoutLineup(match);
        matchWithoutLineup.setSeasonId(match.getSeasonId());
        matchWithoutLineup.setSeason(seasonsService.getSeasonById(match.getSeasonId()));
        Match createdMatch = matchesDao.save(matchWithoutLineup);

        List<Lineup> lineups = getUpdatedLineupsList(createdMatch, match.getLineup());
        List<Lineup> createdLineupList = this.lineupService.createLineups(lineups);

        createdMatch.setLineup(createdLineupList);

        return createdMatch;
    }

    @Transactional
    public Match updateMatch(Match match) {
        if (match.getMatchId() == null) {
            return match;
        }

        match.setLineup(getUpdatedLineupsList(match, match.getLineup()));
        match.setSeasonId(match.getSeasonId());
        match.setSeason(seasonsService.getSeasonById(match.getSeasonId()));

        lineupService.deleteLineupsByMatchId(match.getMatchId());
        return matchesDao.save(match);
    }

    @Transactional
    public void deleteMatch(Integer id) {
        lineupService.deleteLineupsByMatchId(id);
        matchesDao.deleteById(id);
    }

    @Transactional
    public void deleteMatchesBySeasonId(Integer seasonId) {
        List<Match> matches = getMatchesBySeasonId(seasonId);
        for (Match match : matches) {
            lineupService.deleteLineupsByMatchId(match.getMatchId());
        }
        matchesDao.deleteMatchesBySeasonId(seasonId);
    }

    private Match copyMatchWithoutLineup(Match match) {
        Match m = new Match();
        m.setMatchDate(match.getMatchDate() == null ? OffsetDateTime.now() : match.getMatchDate());
        m.setHomeScored(match.getHomeScored());
        m.setAwayScored(match.getAwayScored());

        return m;
    }

    private List<Lineup> getUpdatedLineupsList(Match match, List<Lineup> lineupList) {
        if (lineupList == null) {
            return Collections.EMPTY_LIST;
        }

        for (Lineup l : lineupList) {
            l.setMatch(match);
        }

        return lineupList;
    }

    private void updateBySeasonId(Match match) {
        if (match.getSeasonId() == null && match.getSeason() != null) {
            match.setSeasonId(match.getSeason().getSeasonId());
        }
    }

}
