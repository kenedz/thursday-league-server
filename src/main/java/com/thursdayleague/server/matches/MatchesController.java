package com.thursdayleague.server.matches;

import com.thursdayleague.server.common.annotations.IMatchesRestController;
import com.thursdayleague.server.matches.entities.Match;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@IMatchesRestController
public class MatchesController {

	@Autowired
	private MatchesService matchesService;

	@GetMapping("/filter/{seasonId}")
	public List<Match> getMatchesBySeasonId(@PathVariable("seasonId") Integer seasonId) {
		return matchesService.getMatchesBySeasonId(seasonId);
	}

	@PostMapping
	public Match createMatch(@Valid @RequestBody Match match) {
		return matchesService.createMatch(match);
	}

	@PutMapping
	public Match updateMatch(@Valid @RequestBody Match match) {
		return matchesService.updateMatch(match);
	}

	@DeleteMapping(value = "/{matchId}")
	public void deleteMatch(@PathVariable("matchId") Integer matchId) {
		matchesService.deleteMatch(matchId);
	}

}
