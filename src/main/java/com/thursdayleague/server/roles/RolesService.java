package com.thursdayleague.server.roles;

import com.thursdayleague.server.roles.entities.Role;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RolesService {

	@Autowired
	private RolesDao rolesDao;

	public List<Role> getRoles() {
		return rolesDao.findAll();
	}

}
