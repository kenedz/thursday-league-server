package com.thursdayleague.server.roles;

import com.thursdayleague.server.common.annotations.IRolesRestController;
import com.thursdayleague.server.roles.entities.Role;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;

@IRolesRestController
public class RolesController {

	@Autowired
	private RolesService rolesService;

	@GetMapping()
	public List<Role> getRoles() {
		return rolesService.getRoles();
	}

}
