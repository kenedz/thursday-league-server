package com.thursdayleague.server.roles;

import com.thursdayleague.server.roles.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RolesDao extends JpaRepository<Role, Integer> {
}
