package com.thursdayleague.server.leagues;

import com.thursdayleague.server.leagues.entities.League;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LeaguesService {

    @Autowired
    private LeaguesDao leaguesDao;

    public List<League> getLeagues() {
        return leaguesDao.findAll();
    }

    public League getLeagueById(Integer leagueId) {
        return leaguesDao.getOne(leagueId);
    }

}
