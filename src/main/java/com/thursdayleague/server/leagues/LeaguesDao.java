package com.thursdayleague.server.leagues;

import com.thursdayleague.server.leagues.entities.League;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LeaguesDao extends JpaRepository<League, Integer> {
}
