package com.thursdayleague.server.leagues;

import com.thursdayleague.server.common.annotations.ILeaguesRestController;
import com.thursdayleague.server.leagues.entities.League;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@ILeaguesRestController
public class LeaguesController {

    @Autowired
    private LeaguesService leaguesService;

    @GetMapping()
    public List<League> getLeagues() {
        return leaguesService.getLeagues();
    }

}
