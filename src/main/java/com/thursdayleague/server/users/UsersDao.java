package com.thursdayleague.server.users;

import com.thursdayleague.server.users.entities.User;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersDao extends JpaRepository<User, Integer> {

	String GET_USER_BY_EMAIL = "SELECT * FROM users WHERE email = :email";

	@Query(value = GET_USER_BY_EMAIL, nativeQuery = true)
	List<User> getUser(@Param("email") final String email);

}
