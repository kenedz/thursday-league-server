package com.thursdayleague.server.users;

import com.thursdayleague.server.common.ERoles;
import com.thursdayleague.server.players.PlayersService;
import com.thursdayleague.server.players.entities.Player;
import com.thursdayleague.server.roles.entities.Role;
import com.thursdayleague.server.users.entities.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UsersService {

    @Autowired
    private UsersDao usersDao;

    @Autowired
    private PlayersService playersService;

    private Set<String> emails;

    private void init() {
		if (this.emails == null || this.emails.isEmpty()) {
			this.emails = getEmailsSet(playersService.getPlayers());
		}
    }

    public List<User> getUsers() {
        return usersDao.findAll();
    }

    public User getUser(Integer userId) {
        return usersDao.getOne(userId);
    }

    public List<User> getUserByEmail(String email) {
        return usersDao.getUser(email);
    }

    public User createUser(User user) {
        this.init();

        usersDao.save(user);

        if (!this.emails.contains(user.getEmail())) {
            // otherwise would be possible to add multiple same players when the user would have been removed before and created again
            playersService.createPlayer(convertToPlayer(user));
        }

        return user;
    }

    public void updateUser(User user) {
        usersDao.save(user);
    }

    public void deleteUser(Integer id) {
        User user = getUser(id);
        if (user.getRole() != null && user.getRole().getRole().equals(Role.SUPERADMIN)) {
            return;
        }

        usersDao.deleteById(id);
    }

    public boolean isSuperadmin(User user) {
        boolean isSuperadmin = true;
        if (user != null) {
            Role role = user.getRole();
            if (role == null) {
                User u = getUser(user.getUserId());
                Role r = user.getRole();
                return ERoles.isSuperadmin(r.getRole());
            } else {
                return ERoles.isSuperadmin(role.getRole());
            }
        }

        return !isSuperadmin;
    }

    private Set<String> getEmailsSet(List<Player> players) {
        Set<String> emailsSet = new HashSet<>();

        for (Player player : players) {
            emailsSet.add(player.getEmail());
        }

        return emailsSet;
    }

    private Player convertToPlayer(User user) {
        Player player = new Player();

        player.setEmail(user.getEmail());
        player.setFirstName(user.getFirstName());
        player.setLastName(user.getLastName());

        return player;
    }
}
