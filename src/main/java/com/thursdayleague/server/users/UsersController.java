package com.thursdayleague.server.users;

import com.thursdayleague.server.common.annotations.IUsersRestController;
import com.thursdayleague.server.users.entities.User;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@IUsersRestController
public class UsersController {

	@Autowired
	private UsersService usersService;

	@GetMapping
	public List<User> getUsers() {
		return usersService.getUsers();
	}

	@GetMapping("/filter/{email}")
	public List<User> getUserByEmail(@PathVariable("email") String email) {
		return usersService.getUserByEmail(email);
	}

	@PostMapping
	public User createUser(@Valid @RequestBody User user) {
		return usersService.createUser(user);
	}

	@PutMapping
	public void updateUser(@Valid @RequestBody User user) {
		usersService.updateUser(user);
	}

	@DeleteMapping(value = "/{userId}")
	public void deleteUser(@PathVariable("userId") Integer userId) {
		usersService.deleteUser(userId);
	}

}
