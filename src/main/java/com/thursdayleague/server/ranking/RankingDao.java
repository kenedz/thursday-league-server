package com.thursdayleague.server.ranking;

import com.thursdayleague.server.ranking.entities.Ranking;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RankingDao extends JpaRepository<Ranking, Integer> {

	String GET_RANKINGS_BY_SEASON_ID = "SELECT * FROM players_ranking WHERE season_id = :seasonId";
	String DELETE_RANKINGS_BY_SEASON = "DELETE FROM players_ranking WHERE season_id = :seasonId";

	@Query(value = GET_RANKINGS_BY_SEASON_ID, nativeQuery = true)
	List<Ranking> getRankingsBySeasonId(@Param("seasonId") final Integer seasonId);

	@Modifying
	@Query(value = DELETE_RANKINGS_BY_SEASON, nativeQuery = true)
	void deleteBySeasonId(@Param("seasonId") final Integer seasonId);

}
