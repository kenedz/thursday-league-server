package com.thursdayleague.server.ranking;

import com.thursdayleague.server.ranking.entities.Ranking;
import com.thursdayleague.server.seasons.SeasonsService;
import com.thursdayleague.server.seasons.entities.Season;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class RankingService {

    @Autowired
    private RankingDao rankingDao;

    @Autowired
    private SeasonsService seasonsService;

    public List<Ranking> getRankingsBySeasonId(Integer seasonId) {
        List<Ranking> rankings = rankingDao.getRankingsBySeasonId(seasonId);
        for (Ranking ranking : rankings) {
            updateBySeasonId(ranking);
        }
        return rankings;
    }

    public List<Ranking> createOrUpdateRankings(List<Ranking> rankings) {
        if (rankings == null || rankings.isEmpty()) {
            return rankings;
        }

        final Integer seasonId = rankings.get(0).getSeasonId();
        final Season season = seasonsService.getSeasonById(seasonId);

        if (season == null) {
            return rankings;
        }

        updateRankingsAndAddSeason(rankings, season);
        return rankingDao.saveAll(rankings);
    }

    private void updateRankingsAndAddSeason(final List<Ranking> rankings, final Season season) {
        if (season != null && rankings != null && rankings.size() > 0) {
            final List<Ranking> oldRankings = this.getRankingsBySeasonId(season.getSeasonId());

            if (oldRankings == null || oldRankings.size() == 0) {
                addSeasonToRankings(rankings, season);
            } else if (oldRankings.size() == rankings.size()) {
                for (int i = 0; i < oldRankings.size(); i++) {
                    Ranking oldRanking = oldRankings.get(i);
                    Ranking newRanking = rankings.get(i);
                    newRanking.setRankingId(oldRanking.getRankingId());
                    newRanking.setSeason(season);
                }
            } else {
                Map<String, Ranking> oldRankingsByEmailMap = oldRankings.stream()
                        .collect(Collectors.toMap(ranking -> ranking.getPlayer().getEmail(), ranking -> ranking));

                for (Ranking ranking: rankings) {
                    ranking.setSeason(season);

                    String email = ranking.getPlayer().getEmail();
                    if (oldRankingsByEmailMap.containsKey(email)) {
                        Ranking oldRanking = oldRankingsByEmailMap.get(email);
                        ranking.setRankingId(oldRanking.getRankingId());
                    }
                }
            }

        }
    }

    private void addSeasonToRankings(List<Ranking> rankings, Season season) {
        for (Ranking ranking: rankings) {
            ranking.setSeason(season);
        }
    }

    private void updateBySeasonId(Ranking ranking) {
        if (ranking.getSeasonId() == null && ranking.getSeason() != null) {
            ranking.setSeasonId(ranking.getSeason().getSeasonId());
        }
    }
}
