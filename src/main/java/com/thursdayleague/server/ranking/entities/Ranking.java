package com.thursdayleague.server.ranking.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.thursdayleague.server.players.entities.Player;
import com.thursdayleague.server.seasons.entities.Season;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "players_ranking")
public class Ranking implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ranking_id", nullable = false)
    private Integer rankingId;

    @Column(name = "rank")
    private Integer rank;

    @Column(name = "points_avg")
    private Double pointsAvg;

    @Column(name = "level")
    private Integer level;

    @Column(name = "points_overall")
    private Integer pointsOverall;

    @Column(name = "played")
    private Integer played;

    @Column(name = "missed")
    private Integer missed;

    @Column(name = "wins")
    private Integer wins;

    @Column(name = "draws")
    private Integer draws;

    @Column(name = "loses")
    private Integer loses;

    @Column(name = "scored_overall")
    private Integer scoredOverall;

    @Column(name = "scored_avg")
    private Double scoredAvg;

    @Column(name = "received_overall")
    private Integer receivedOverall;

    @Column(name = "received_avg")
    private Double receivedAvg;

    @OneToOne
    @JoinColumn(name = "player_id", nullable = false)
    private Player player;

    @ManyToOne
    @JoinColumn(name = "season_id", nullable = false)
    @JsonIgnore
    private Season season;

    @Transient
    private Integer seasonId;

    public Integer getRankingId() {
        return rankingId;
    }

    public void setRankingId(Integer rankingId) {
        this.rankingId = rankingId;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer order) {
        this.rank = order;
    }

    public Double getPointsAvg() {
        return pointsAvg;
    }

    public void setPointsAvg(Double pointsAvg) {
        this.pointsAvg = pointsAvg;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getPointsOverall() {
        return pointsOverall;
    }

    public void setPointsOverall(Integer pointsOverall) {
        this.pointsOverall = pointsOverall;
    }

    public Integer getPlayed() {
        return played;
    }

    public void setPlayed(Integer played) {
        this.played = played;
    }

    public Integer getMissed() {
        return missed;
    }

    public void setMissed(Integer missed) {
        this.missed = missed;
    }

    public Integer getWins() {
        return wins;
    }

    public void setWins(Integer wins) {
        this.wins = wins;
    }

    public Integer getDraws() {
        return draws;
    }

    public void setDraws(Integer draws) {
        this.draws = draws;
    }

    public Integer getLoses() {
        return loses;
    }

    public void setLoses(Integer loses) {
        this.loses = loses;
    }

    public Integer getScoredOverall() {
        return scoredOverall;
    }

    public void setScoredOverall(Integer scoredOverall) {
        this.scoredOverall = scoredOverall;
    }

    public Double getScoredAvg() {
        return scoredAvg;
    }

    public void setScoredAvg(Double scoredAvg) {
        this.scoredAvg = scoredAvg;
    }

    public Integer getReceivedOverall() {
        return receivedOverall;
    }

    public void setReceivedOverall(Integer receivedOverall) {
        this.receivedOverall = receivedOverall;
    }

    public Double getReceivedAvg() {
        return receivedAvg;
    }

    public void setReceivedAvg(Double receivedAvg) {
        this.receivedAvg = receivedAvg;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Season getSeason() {
        return season;
    }

    public void setSeason(Season season) {
        this.season = season;
    }

    public Integer getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(Integer seasonId) {
        this.seasonId = seasonId;
    }
}
