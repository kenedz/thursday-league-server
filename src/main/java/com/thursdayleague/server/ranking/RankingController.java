package com.thursdayleague.server.ranking;

import com.thursdayleague.server.common.annotations.IRankingRestController;
import com.thursdayleague.server.ranking.entities.Ranking;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

import javax.validation.Valid;

@IRankingRestController
public class RankingController {

    @Autowired
    private RankingService rankingService;

    @GetMapping("/rankings/{seasonId}")
    public List<Ranking> getRankingsBySeasonId(@PathVariable("seasonId") Integer seasonId) {
        return rankingService.getRankingsBySeasonId(seasonId);
    }

    @PostMapping("/rankings")
    public List<Ranking> createOrUpdateRankings(@Valid @RequestBody List<Ranking> rankings) {
        return rankingService.createOrUpdateRankings(rankings);
    }

}
