package com.thursdayleague.server.players;

import com.thursdayleague.server.players.entities.Player;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayersDao  extends JpaRepository<Player, Integer> {

	String GET_PLAYER_BY_EMAIL = "SELECT * FROM players WHERE email = :email";

	@Query(value = GET_PLAYER_BY_EMAIL, nativeQuery = true)
	List<Player> getPlayer(@Param("email") final String email);

}
