package com.thursdayleague.server.players;

import com.thursdayleague.server.players.entities.Player;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlayersService {

    @Autowired
    private PlayersDao playersDao;

    public List<Player> getPlayers() {
        return playersDao.findAll();
    }

    public Player createPlayer(Player player) {
        return playersDao.save(player);
    }

}
