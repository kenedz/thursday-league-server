package com.thursdayleague.server.players;

import com.thursdayleague.server.common.annotations.IPlayersRestController;
import com.thursdayleague.server.players.entities.Player;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@IPlayersRestController
public class PlayersRestController {

    @Autowired
    private PlayersService playersService;

    @GetMapping
    public List<Player> getPlayers() {
        return playersService.getPlayers();
    }

}
