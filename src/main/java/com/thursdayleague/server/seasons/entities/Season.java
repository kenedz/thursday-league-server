package com.thursdayleague.server.seasons.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.thursdayleague.server.leagues.entities.League;
import com.thursdayleague.server.matches.entities.Match;
import com.thursdayleague.server.ranking.entities.Ranking;

import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "seasons")
public class Season implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "season_id", nullable = false)
    private Integer seasonId;

    @Column(name = "season_year")
    private String seasonYear;

    @Column(name = "season_start")
    private OffsetDateTime seasonStart;

    @Column(name = "season_end")
    private OffsetDateTime seasonEnd;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "league_id", nullable = false)
    private League league;

    @Transient
    private Integer leagueId;

    @OneToMany(mappedBy = "season", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Match> matches;

    @OneToMany(mappedBy = "season", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Ranking> rankings;

    public Integer getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(Integer seasonId) {
        this.seasonId = seasonId;
    }

    public String getSeasonYear() {
        return seasonYear;
    }

    public void setSeasonYear(String seasonYear) {
        this.seasonYear = seasonYear;
    }

    public OffsetDateTime getSeasonStart() {
        return seasonStart;
    }

    public void setSeasonStart(OffsetDateTime seasonStart) {
        this.seasonStart = seasonStart;
    }

    public OffsetDateTime getSeasonEnd() {
        return seasonEnd;
    }

    public void setSeasonEnd(OffsetDateTime seasonEnd) {
        this.seasonEnd = seasonEnd;
    }

    public Set<Match> getMatches() {
        return matches;
    }

    public void setMatches(Set<Match> matches) {
        this.matches = matches;
    }

    public Set<Ranking> getRankings() {
        return rankings;
    }

    public void setRankings(Set<Ranking> rankings) {
        this.rankings = rankings;
    }

    public League getLeague() {
        return league;
    }

    public void setLeague(League league) {
        this.league = league;
    }

    public Integer getLeagueId() {
        return leagueId;
    }

    public void setLeagueId(Integer leagueId) {
        this.leagueId = leagueId;
    }
}
