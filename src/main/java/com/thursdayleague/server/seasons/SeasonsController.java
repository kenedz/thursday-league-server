package com.thursdayleague.server.seasons;

import com.thursdayleague.server.common.annotations.ISeasonsRestController;
import com.thursdayleague.server.matches.entities.Match;
import com.thursdayleague.server.seasons.entities.Season;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

import javax.validation.Valid;

@ISeasonsRestController
public class SeasonsController {

    @Autowired
    private SeasonsService seasonsService;

    @GetMapping()
    public List<Season> getSeasons() {
        return seasonsService.getSeasons();
    }

    @GetMapping("/season/{seasonId}")
    public Season getSeasonById(@PathVariable("seasonId") Integer seasonId) {
        return seasonsService.getSeasonById(seasonId);
    }

    @GetMapping("/league/{leagueId}")
    public List<Season> getSeasonByLeagueId(@PathVariable("leagueId") Integer leagueId) {
        return seasonsService.getSeasonByLeagueId(leagueId);
    }

    @PostMapping
    public Season createSeason(@Valid @RequestBody Season season) {
        return seasonsService.createOrUpdateSeason(season);
    }

    @PutMapping
    public Season updateSeason(@Valid @RequestBody Season season) {
        return seasonsService.createOrUpdateSeason(season);
    }

    @DeleteMapping(value = "/{seasonId}")
    public void deleteMatch(@PathVariable("seasonId") Integer seasonId) {
        seasonsService.deleteSeason(seasonId);
    }

}
