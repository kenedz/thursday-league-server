package com.thursdayleague.server.seasons;

import com.thursdayleague.server.seasons.entities.Season;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SeasonsDao extends JpaRepository<Season, Integer> {

    String GET_SEASONS_BY_LEAGUE_ID = "SELECT * FROM seasons WHERE league_id = :leagueId";

    @Query(value = GET_SEASONS_BY_LEAGUE_ID, nativeQuery = true)
    List<Season> getSeasonByLeagueId(@Param("leagueId") final Integer leagueId);

}
