package com.thursdayleague.server.seasons;

import com.thursdayleague.server.leagues.LeaguesService;
import com.thursdayleague.server.matches.MatchesService;
import com.thursdayleague.server.seasons.entities.Season;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.transaction.Transactional;

@Service
public class SeasonsService {

    @Autowired
    private SeasonsDao seasonsDao;

    @Autowired
    private MatchesService matchesService;

    @Autowired
    private LeaguesService leaguesService;

    public List<Season> getSeasons() {
        List<Season> seasons = seasonsDao.findAll();
        for (Season season: seasons) {
            updateByLeagueId(season);
        }
        return seasons;
    }

    public Season getSeasonById(Integer seasonId) {
        final Season season = seasonsDao.getOne(seasonId);
        updateByLeagueId(season);
        return season;
    }

    public List<Season> getSeasonByLeagueId(Integer leagueId) {
        final List<Season> seasons = seasonsDao.getSeasonByLeagueId(leagueId);
        for (Season season: seasons) {
            updateByLeagueId(season);
        }

        return seasons;
    }

    public Season createOrUpdateSeason(Season season) {
        season.setLeague(leaguesService.getLeagueById(season.getLeagueId()));
        return this.seasonsDao.save(season);
    }

    @Transactional
    public void deleteSeason(Integer seasonId) {
        this.matchesService.deleteMatchesBySeasonId(seasonId);
        this.seasonsDao.deleteById(seasonId);
    }

    private void updateByLeagueId(Season season) {
        if (season.getLeagueId() == null && season.getLeague() != null) {
            season.setLeagueId(season.getLeague().getLeagueId());
        }
    }
}
