package com.thursdayleague.server.matcheslineup.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.thursdayleague.server.matches.entities.Match;
import com.thursdayleague.server.players.entities.Player;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "matches_lineup")
public class Lineup implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "lineup_id", nullable = false)
    private Integer lineupId;

    // TODO ZT co to upravit na OneToOne Lineup <--> Match a Lineup bude mit OneToMany na Players??
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "match_id", nullable = false)
    private Match match;

    @Transient
    private Integer matchId;

    @OneToOne
    @JoinColumn(name = "player_id", nullable = false)
    private Player player;

    @Column(name = "team", nullable = false)
    private String team;

    public Integer getLineupId() {
        return lineupId;
    }

    public void setLineupId(Integer lineupId) {
        this.lineupId = lineupId;
    }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public Integer getMatchId() {
        return matchId;
    }

    public void setMatchId(Integer matchId) {
        this.matchId = matchId;
    }
}
