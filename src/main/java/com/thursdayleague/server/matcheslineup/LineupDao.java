package com.thursdayleague.server.matcheslineup;

import com.thursdayleague.server.matcheslineup.entities.Lineup;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface LineupDao  extends JpaRepository<Lineup, Integer> {

	String LINEUP_BY_MATCH_ID = "SELECT * FROM matches_lineup WHERE match_id = :matchId";
	String DELETE_BY_MATCH_ID = "DELETE FROM matches_lineup WHERE match_id = :matchId";

	@Query(value = LINEUP_BY_MATCH_ID, nativeQuery = true)
	List<Lineup> getLineupByMatchId(@Param("matchId") Integer matchId);

	@Modifying
	@Query(value = DELETE_BY_MATCH_ID, nativeQuery = true)
	void deleteLineupsByMatchId(@Param("matchId") Integer matchId);

}
