package com.thursdayleague.server.matcheslineup;

import com.thursdayleague.server.matcheslineup.entities.Lineup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LineupService {

    @Autowired
    private LineupDao lineupDao;

    public Lineup createLineup(Lineup lineup) {
        return lineupDao.save(lineup);
    }

    public List<Lineup> createLineups(List<Lineup> lineups) {
        List<Lineup> lineupList = new ArrayList();

        for (Lineup lineup : lineups) {
            lineupList.add(createLineup(lineup));
        }

        return lineupList;
    }

    public void deleteLineupsByMatchId(Integer matchId) {
        lineupDao.deleteLineupsByMatchId(matchId);
    }

}
