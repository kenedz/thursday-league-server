package com.thursdayleague.server.common.configuration;

import java.util.Arrays;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Controller
public class WebConfig {

	@Value("${cors.allowedOrigins}")
	private String[] allowedOrigins;

	@Value("${cors.allowedCredentials}")
	private Boolean allowedCredentials;

	@Value("${cors.allowedMethods}")
	private String[] allowedMethods;

	@Value("${cors.allowedHeaders}")
	private String[] allowedHeaders;

	@Bean
	public CorsFilter corsFilter() {
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowedOrigins(Arrays.asList(allowedOrigins));
		config.setAllowCredentials(allowedCredentials);
		config.setAllowedMethods(Arrays.asList(allowedMethods));
		config.setAllowedHeaders(Arrays.asList(allowedHeaders));

		UrlBasedCorsConfigurationSource configurationSource = new UrlBasedCorsConfigurationSource();
		configurationSource.registerCorsConfiguration("/**", config);

		return new CorsFilter(configurationSource);
	}

}
