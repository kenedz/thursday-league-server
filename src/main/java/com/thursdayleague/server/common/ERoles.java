package com.thursdayleague.server.common;

public enum ERoles {

	SUPERADMIN(1, "superadmin"),
	ADMIN(2, "admin"),
	USER(3, "user");

	private Integer id;
	private String role;

	ERoles(Integer id, String role) {
		this.id = id;
		this.role = role;
	}

	public static boolean isSuperadmin(String role) {
		return role.equals(SUPERADMIN.role);
	}

}
