package com.thursdayleague.server.users;

import com.thursdayleague.server.roles.entities.Role;
import com.thursdayleague.server.users.entities.User;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class UsersServiceTest {

	@Autowired
	private UsersService usersService;

	@Test
	void isSuperadmin() {
		Role role1 = new Role();
		role1.setRoleId(1);
		role1.setRole("superadmin");

		Role role2 = new Role();
		role2.setRoleId(2);
		role2.setRole("admin");

		Role role3 = new Role();
		role3.setRoleId(3);
		role3.setRole("user");

		User user = new User();
		user.setRole(role1);
		assertEquals(true, usersService.isSuperadmin(user));

		user.setRole(role2);
		assertEquals(false, usersService.isSuperadmin(user));

		user.setRole(role3);
		assertEquals(false, usersService.isSuperadmin(user));
	}
}
